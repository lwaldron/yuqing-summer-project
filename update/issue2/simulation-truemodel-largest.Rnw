\documentclass{article}

\begin{document}
\SweaveOpts{concordance=TRUE}

\section{Intro--Load Libraries and Functions}
<<echo=T,results=hide>>=
library(simulatorZ)
setwd("~/workspace/Validations")
load("ExpressionSetsList_rm_new.RData")
step <- 100
largeN <- 150
iterations <- 100
balance.variables <- c("size", "age", "node", "grade")
fold <- 4
set.seed(732)
y.vars <- list()
for(i in 1:length(esets)){  
  time <- esets[[i]]$dmfs.time
  time <- as.numeric(as.character(time))
  cens <- esets[[i]]$dmfs.cens
  cens <- as.numeric(as.character(cens))
  y.vars[[i]] <- Surv(time, cens)
}
pfactor <- 1
@

\section{Using the same true model}
<<boxplots-same-model,echo=TRUE,fig=TRUE>>=
Z.list <- list()
CV <- CSV <- c()
for(b in 1:iterations){
  print(paste("iteration: ", b, sep=""))
  
  ## get true model
  truemod <- getTrueModel(esets[3], y.vars[3], parstep=step)
  simmodels <- simData(esets, 150, y.vars)
  beta <- grid <- survH <- censH <- lp <- list()
  for(listid in 1:length(esets)){
    beta[[listid]] <- truemod$beta[[1]] * pfactor
    grid[[listid]] <- truemod$grid[[1]]
    survH[[listid]] <- truemod$survH[[1]]
    censH[[listid]] <- truemod$censH[[1]]
    lp[[listid]] <- (truemod$beta[[1]]*pfactor) %*% exprs(esets[[listid]])
  }
  res <- list(beta=beta, grid=grid, survH=survH, censH=censH, lp=lp)
  simmodels <- simTime(simmodels, res)
  
  ## generate z matrix
  Z.list[[b]] <- zmatrix(obj=simmodels$obj, 
                         y.vars=simmodels$y.vars, fold=fold,
                         trainingFun=plusMinus)
  sum.cv <- 0
  for(i in 1:length(esets)){
    sum.cv <- sum.cv + Z.list[[b]][i, i]
  }
  CV[b] <- sum.cv / length(esets)
  CSV[b] <- (sum(Z.list[[b]]) - sum.cv) / (length(esets)*(length(esets)-1))
}

resultlist <- list(CSV=CSV, CV=CV)
boxplot(resultlist, col=c("white", "grey"), ylab="C-Index", boxwex = 0.25, xlim=c(0.5, 2.5), ylim=c(0.5, 0.7))

result_test <- resultlist
save(result_test, file="test_truemodel.RData")
@

\end{document}