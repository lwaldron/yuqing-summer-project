\documentclass{article}

\begin{document}
\SweaveOpts{concordance=TRUE}

\section{Intro--Load Libraries and Functions}
<<echo=T,results=hide>>=
require(survival)  
require(pls)
require(CoxBoost)
require(Biobase)
require(survHD)
require(gbm)
require(Hmisc)
library(superpc)
library(gplots)
library(ClassDiscovery)
library(meta)
setwd("~/workspace/scripts/test6-updatefunctionsandimplementation")
load("ExpressionSetsList_rm_new.RData")
source("simData.R")
source("getTrueModel.R")
source("simTime.R")
source("simBootstrap.R")
source("plusminuscore.R")
source("masomenos.R")
source("cvSubsets.R")
source("funCV.R")
source("zmatrix.R")
source("geneFilter.R")
step <- 100
balance.variables <- c("size", "age", "node", "grade")
largeN <- 150
iterations <- 100
fold <- 4
set.seed(732)
y.vars <- list()
for(i in 1:length(esets)){  
  time <- esets[[i]]$dmfs.time
  time <- as.numeric(as.character(time))
  cens <- esets[[i]]$dmfs.cens
  cens <- as.numeric(as.character(cens))
  y.vars[[i]] <- Surv(time, cens)
}
@

\section{pgr analysis}
<<pgr,echo=TRUE,fig=TRUE>>=
load("ExpressionSetsList_rm_new_pgr.RData")
esets <- esets[-1]
y.vars <- list()
for(i in 1:length(esets)){  
  time <- esets[[i]]$dmfs.time
  time <- as.numeric(as.character(time))
  cens <- esets[[i]]$dmfs.cens
  cens <- as.numeric(as.character(cens))
  y.vars[[i]] <- Surv(time, cens)
}
Z.list <- list()
CV.balance <- CSV.balance <- CV.notbalance <- CSV.notbalance<- c()

### Not balancing the covariates
for(b in 1:iterations){
  print(paste("iteration: ", b, sep=""))
  sim2.esets <- simBootstrap(esets=esets, y.vars=y.vars, n.samples=largeN, 
                             parstep=step, type="two-steps")
  Z.list[[b]] <- zmatrix(esets=sim2.esets$esets.list, 
                         y.vars=sim2.esets$y.vars.list, fold=fold,
                         trainingFun=plusMinus)
  sum.cv <- 0
  for(i in 1:length(esets)){
    sum.cv <- sum.cv + Z.list[[b]][i, i]
  }
  CV.notbalance[b] <- sum.cv / length(esets)
  CSV.notbalance[b] <- (sum(Z.list[[b]]) - sum.cv) / (length(esets)*(length(esets)-1))
}
### balancing the covariates
for(b in 1:iterations){
  print(paste("iteration: ", b, sep=""))
  sim3.esets <- simBootstrap(esets=esets, y.vars=y.vars,
                             n.samples=largeN, parstep=step,
                           type="two-steps", balance.variables="pgr")
  Z.list[[b]] <- zmatrix(esets=sim3.esets$esets.list, 
                         y.vars=sim3.esets$y.vars.list, fold=fold,
                         trainingFun=plusMinus)
  sum.cv <- 0
  for(i in 1:length(esets)){
    sum.cv <- sum.cv + Z.list[[b]][i, i]
  }
  CV.balance[b] <- sum.cv / length(esets)
  CSV.balance[b] <- (sum(Z.list[[b]]) - sum.cv) / (length(esets)*(length(esets)-1))
}

CVlist <- list(CV.notbalance, CV.balance)
names(CVlist) <- c("imbalance", "balance")
CSVlist <- list(CSV.notbalance, CSV.balance)
names(CSVlist) <- c("imbalance", "balance")
boxplot(CSVlist, col=c("white", "white"), ylab="value", boxwex = 0.25, at = 1:2 - 0.2, xlim=c(0.5, 2.5), ylim=c(0.4, 0.7))
boxplot(CVlist, col=c("grey", "grey"), ylab="value", add=TRUE, boxwex = 0.25, at = 1:2 + 0.2, main="Balancing pgr")
par(new=TRUE)
y=seq(0.4, 0.7, by=0.0001)
lines(x=rep(1.5, length(y)), y=y, type="l", lty=2)
legend("bottomright", col=c("grey", "black"), legend=c("CV", "CSV"), pch=c(16,1))
@

\end{document}