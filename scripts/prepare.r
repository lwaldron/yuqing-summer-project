### start:head working directory is the directory to which
### Benjamin Haibe Kain's 'data.zip' has been extracted. It
### contains nothing but these RData-files.
setwd("~/workspace/data/")
scriptpath <- "~/workspace/scripts/"  ##directory including scripts (e.g. lintrans_cutoff.r)

### fetching lintrans_cutoff.R
#url <- "http://bitbucket.org/bernau/zranking/"
#download.file(url=url, destfile=scriptpath)

outputdir <- "~/workspace/prepared/"  ##directory for output
rm_lowvar <- 0.6  #portion of low variance genes to be removed (0-> remove later)
### prepared data sets will be saved in ../prepared (relative
### to wd, please create this folder if it does not exist)
erst <- 1  #er-status to remain in study
source(paste(scriptpath, "lintrans_cutoff.r", sep = ""))
### required packages
library(WGCNA)  #bioconductor
library(survival)  #cran (preinstalled)
library(pls)  #cran
#### end:head

### start:step 1 (merging on gene symbol level, creating
### response list) names of the data set
setnamesind <- c(1, 17, 21, 29, 30, 31, 34, 36)
setnames <- dir()[setnamesind]

### which data sets contain information on rfs,os,dmfs?
dmfsind <- rfsind <- osind <- erind <- numeric(length(setnames))

ii <- 1
for (i in setnames) {
	load(i)
	if (class(demo)!="function") {
		print(i)
		print("nodes")
		try(print(ftable(demo$node, demo$er)))
		print(table(demo$e.dmfs))
		if (length(table(demo$e.dmfs) > 1)) 
			dmfsind[ii] <- 1
    else dmfsind[ii] <- 0
		if (length(table(demo$e.rfs) > 1)) 
			rfsind[ii] <- 1
    else rfsind[ii] <- 0
		if (length(table(demo$e.os) > 1)) 
			osind[ii] <- 1	
    else osind[ii] <- 0
		if (length(table(demo$er) > 1)) 
			erind[ii] <- 1
    else erind[ii] <- 0
		print(table(demo$er))
	}
	ii <- ii + 1
	rm(demo)
}

### how many data sets contain information on dmfs,rfs,os,er?
sum(dmfsind)
sum(rfsind)
sum(osind)
sum(erind)


### lists for expression matrices on gene symbol level, and
### Surv-objects for dmfs,rfs & os
geneXL <- list()
dmfsYL <- list()
rfsYL <- list()
osYL <- list()

ii <- 1
for (i in setnames) {
	print(i)
	load(i)
	if (class(demo) != "function") {
		if (is.null(demo$er) != TRUE) {
			# get samples that match the ER status
      indsER <- which(demo$er == erst)
			# get gene symbol information
			try(gensymb <- annot$Gene.symbol)
			# get id variable for collapseRows()
			try(genid <- colnames(data))
			# remove expressions with gene symbol=NA (recommended by authors of WGCNA)
			try(indsOK <- which(is.na(gensymb) == FALSE & is.na(genid) == FALSE))
			# merge on gene level using collapse rows
			try(newX <- t(collapseRows(datET = t(data[indsER, 
													indsOK]), rowGroup = gensymb[indsOK], rowID = genid[indsOK], 
									method = "MaxMean")$datETcollapsed))
      rname <- row.names(newX)
      try(newdemo <- demo[rname, ])
			# save expression on gene level in separate data set
			# try(geneXL[[ii]]<-newX)
			try(newX <- t(linearTransform(t(newX))))
			try(save(newX, newdemo, file = paste(outputdir, "prepared_", 
									i, ".RData", sep = "")))
			### free memory
			rm(newX)
			# save different survival responses in corresponding lists or
			# set NA if missing
			if (osind[ii] == 1) {
				osYL[[ii]] <- Surv(demo$t.os, demo$e.os)[indsER,]
			} 
      else osYL[[ii]] <- NA
			if (rfsind[ii] == 1) {
				rfsYL[[ii]] <- Surv(demo$t.rfs, demo$e.os)[indsER,]
			} 
      else rfsYL[[ii]] <- NA
			if (dmfsind[ii] == 1) {
				dmfsYL[[ii]] <- Surv(demo$t.dmfs, demo$e.dmfs)[indsER,]
			} 
      else dmfsYL[[ii]] <- NA
		} 
    else {
			osYL[[ii]] <- dmfsYL[[ii]] <- rfsYL[[ii]] <- NA
		}
	  } 
  else {
		osYL[[ii]] <- dmfsYL[[ii]] <- rfsYL[[ii]] <- NA
	}
	ii <- ii + 1
	try(rm(data))
	try(rm(demo))
}

### save all important objects for subsequent analysis
save(dmfsYL, osYL, rfsYL, rfsind, osind, dmfsind, erind, setnames, 
		file = paste(outputdir, "XYzranking.RData", sep = ""))

### check overlap of gene-symbols

### end: step1

### start: step2 (check overlap of genes)
load(paste(outputdir, "XYzranking.RData", sep = ""))

# list of genesymbols
genesymbollist <- list()

ii <- 1
for (i in setnames) {
	try(load(file = paste(outputdir, "prepared_", i, ".RData",sep = "")), silent = T)
	# get genesymbols
	ch <- try(genesymbollist[[ii]] <- colnames(newX), silent = T)
	if (class(ch) != "try-error") {
		# print(names[ii])
		print(nrow(newX))
		# print(print(nrow(na.omit(newX)))) print(mean(newX))
	}
	# free mem
	try(rm(newX), silent = T)
	ii <- ii + 1
}

### check overlap
N <- length(genesymbollist)
overlap <- matrix(ncol = N, nrow = N)

for (ii in 1:N) for (jj in 1:N) try(overlap[ii, jj] <- length(intersect(genesymbollist[[ii]], 
								genesymbollist[[jj]])))
unique(as.numeric(overlap))
overlap[1, ]
seq.intersect <- function(seq, list) {
	newinter <- list[[seq[1]]]
	for (index in 2:length(seq)) newinter <- intersect(newinter, 
				list[[seq[index]]])
	print(length(newinter))
	return(newinter)
}

seq <- 1:8
genesymb <- seq.intersect(seq, genesymbollist)
length(seq)
### overlap of 8839 for these 25 data sets
booloverlap <- numeric(N)
booloverlap[seq] <- 1
# save(seqoverlap,booloverlap,genesymb,file='../prepared/seqoverlap.RData')
save(booloverlap, genesymb, file = paste(outputdir, "seqoverlap.RData",sep = ""))

#### how many data sets can be used for different outcomes
length(which(dmfsind == 1 & booloverlap == 1))
length(which(rfsind == 1 & booloverlap == 1))
length(which(osind == 1 & booloverlap == 1))

## check length of these 25 data sets

## end:step2 start:step3:
## remove low variance genes

seq <- 1:8
vars <- list()
listind <- 1
### determine variance of all genes in all datasets
for (i in seq) {
	load(paste(outputdir, "prepared_", setnames[i], ".RData", sep = ""))
	X <- newX[ ,match(genesymb, colnames(newX))]
	# print(nrow(X)) print(ncol(X))
	vv <- list(variances = apply(X, 2, var), N = nrow(X))
	vars[[listind]] <- vv
	listind <- listind + 1
}


## average variance of genes across datasets
av.vars <- vars[[1]][[1]]
for (i in c(1:length(seq))) if (any(is.na(vars[[i]][[1]])) == FALSE) av.vars <- av.vars + vars[[i]][[1]]
av.vars <- av.vars/length(seq)


# save average variances for future use
save(av.vars, file = paste(outputdir, "av_vars.RData", sep = ""))

highvarinds <- which(av.vars >= quantile(av.vars, rm_lowvar))
### save data sets with high variance genes only
for (i in seq) {
	load(paste(outputdir, "prepared_", setnames[i], ".RData", sep = ""))
	X <- newX[, match(genesymb, colnames(newX))]
	newX <- X[, highvarinds]
	print(setnames[i])
	save(newX, newdemo, file = paste(outputdir, "prepared_rm_", setnames[i], 
					".RData", sep = ""))
}

### check (not run): load prepared datasets and check overlap
### of genes
if (1 == 0) {
	listind <- 1
	gensyml <- list()
	for (i in seq) {
		print(setnames[i])
		load(paste(outputdir, "prepared_rm_", setnames[i], ".RData", 
						sep = ""))
		gensyml[[listind]] <- colnames(newX)
		if (listind != 1) {
			overlapping <- intersect(gensyml[[listind - 1]], 
					gensyml[[listind]])
			print(length(overlapping))
			print(length(colnames(newX)))
		}
		listind <- listind + 1
	}
}
### end: step 3 

### additional step: fill in missing values
#load(paste(outputdir, "XYzranking.RData", sep=""))
#for(j in 1:2){
#  for(i in 1:8){
#    id <- which(is.na(dmfsYL[[i]][, j]) == TRUE)
#    if(length(id) > 0){
#      print("yes")
#      if(j == 1) fillinvalue <- mean(dmfsYL[[i]][-id, 1])  
#      else fillinvalue <- 0
#      dmfsYL[[i]][id, j] <- rep(fillinvalue, length(id))
#    }
#  }
#}

#save(dmfsYL, osYL, rfsYL, rfsind, osind, dmfsind, erind, setnames, 
#     file = paste(outputdir, "XYzranking.RData", sep = ""))


#for(i in 1:8){  
#  load(paste(outputdir, "prepared_rm_", setnames[i], ".RData", sep = ""))
#  for(j in 1:length(genesymb)){
#    id <- which(is.na(newX[, j]) == TRUE)
#    if(length(id) > 0){
#      fillinvalue <- mean(newX[-id, j]) 
#      newX[id, j] <- rep(fillinvalue, length(id))      
#    }
#  }
#  #id <- which(is.na(newdemo[, c("grade")]) == TRUE)
#  #if(length(id) > 0) {
#  #  print(i)
#  #  fillinvalue <- mean(newdemo[-id, c("grade")]) 
#  #  newdemo[id, c("grade")] <- rep(fillinvalue, length(id))
#  #}
#  save(newX, newdemo, file = paste(outputdir, "prepared_rm_", setnames[i], 
#                               ".RData", sep = ""))
#}



### Excluding the situation where censored survival time == 0
#for(i in 1:8){
#  load(paste(outputdir, "prepared_rm_", setnames[i], ".RData", sep = ""))
#  indno <- which(dmfsYL[[i]][, 1] == 0)
#  if(length(indno) > 0) {
#    newX <- newX[-indno, ]
#    newdemo <- newdemo[-indno, ]
#    dmfsYL[[i]] <- dmfsYL[[i]][-indno, ]
#    save(newX, newdemo, file = paste(outputdir, "prepared_rm_", setnames[i], 
#                                     ".RData", sep = ""))
#  }
#}

#save(dmfsYL, osYL, rfsYL, rfsind, osind, dmfsind, erind, setnames, 
#     file = paste(outputdir, "XYzranking.RData", sep = ""))